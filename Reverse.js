var reverseWords = function() {
   var line;

   while (line = readline()) {
      print(line.trim().split(" ").reverse().join(" "));
   }
};

reverseWords();
