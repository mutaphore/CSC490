var countNames = function() {

   var line, list = {}, temp;
   while ((line = readline().trim().toUpperCase()) !== "") {
      if (list[line] === undefined)
         list[line] = 1;
      else
         list[line]++;
   }

   while ((line = readline().trim()) !== "") {
      temp = line.toUpperCase();
      if (list[temp] !== undefined)
         print(line + "  appears  " + list[temp] + "  times.")
      else
         print(line + "  appears  0  times.")
   }
};
countNames();
