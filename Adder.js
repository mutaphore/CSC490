load("LinkedList4.js");

var makeAdder = function(incr) {
   return function(val) {return val + incr;}
};

(function() {
   var list = new LinkedList();
   var adder = makeAdder(10);

   [42, 14, 100].forEach(function(val) {list.add(val);});
   list.mapEach(adder);
   list.forEach(function(val) {print(val);});
})();

// //Adder using closure concepts
// (function() {
//    var list = new LinkedList();
//    //var adder = makeAdder(10);

//    [42, 14, 100].forEach(function(val) {list.add(val);});
//    (function(incr) {
// 		return function(val) {return val + incr};
// 	})(10);
//    list.mapEach(x);
//    list.forEach(function(val) {print(val);});
// })();