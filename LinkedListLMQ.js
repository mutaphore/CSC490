var LinkedList = function() {
   this.head = null;

   this.add = function(val) {
      this.head = {data: val, next: this.head};
   };

   this.getIterator = function() {
      return new LinkedList.Iterator(this.head);
   }
};

LinkedList.Iterator = function(head) {
   var pos = head;

   this.current = function() {
      return pos.data;
   }

   this.isDone = function() {
      return pos === null;
   }

   this.advance = function() {
      pos = pos.next;
   }

   this.reset = function() {
      pos = head;
   }
};



(function() {
   var cmd, itr, lists = [new LinkedList(), new LinkedList()];

   while ((cmd = readline().split(/\s+/)).length) {
      if (cmd[0] === 'add')
         lists[cmd[1]].add(cmd[2]);
      else if (cmd[0] === 'print') {
         for (itr = lists[cmd[1]].getIterator(); !itr.isDone(); itr.advance())
            print(itr.current());
         print("And again...");
         for (itr.reset(); !itr.isDone(); itr.advance())
            print(itr.current());
      }
      else if (cmd[0] === 'quit')
         break;
   }
})();
