q = [];
var pushTail = q.push;
var pushHead = q.unshift;
var popTail = q.pop;
var popHead = q.shift;

var isEmpty = function() {
   return !length;
};

var makeDeque = function() {
   return this;
};

(function() {
   var dq = makeDeque();

   dq.pushTail(4);
   dq.pushHead(3);
   dq.pushHead(2);
   dq.pushHead("one");
   dq.pushTail("five");
   print("length " + dq.length + "last item: " + dq.popTail());
   while (!dq.isEmpty())
      print(dq.popHead());
})();
