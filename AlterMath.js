var oldSin = Math.sin;
var oldCos = Math.cos;
var oldTan = Math.tan;

//(function() {
   Math.sin = function() {
      var rad = arguments[0], ans;

      if (arguments.length === 2 && arguments[1] === "d")
         rad = rad * Math.PI / 180;

      (function() {ans = oldSin(rad)})();
      return ans;
   }
   Math.cos = function() {
      var rad = arguments[0], ans;

      if (arguments.length === 2 && arguments[1] === "d")
         rad = rad * Math.PI / 180;

      (function() {ans = oldCos(rad)})();
      return ans;
   }
   Math.tan = function() {
      var rad = arguments[0], ans;

      if (arguments.length === 2 && arguments[1] === "d")
         rad = rad * Math.PI / 180;

      (function() {ans = oldTan(rad)})();
      return ans;
   }
//})();

(function() {
   var angle;

   while (angle = parseFloat(readline())) {
      print(Math.sin(angle, "d").toPrecision(5));  // degrees
      print(Math.sin(angle).toPrecision(5));       // radians
      print(Math.cos(angle, "d").toPrecision(5));
      print(Math.cos(angle).toPrecision(5));
      print(Math.tan(angle, "d").toPrecision(5));
      print(Math.tan(angle).toPrecision(5));
   }
})();
