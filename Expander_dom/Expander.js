var Expander = {};

Expander.showhide = function(item, title) {
  return function() {
    if (item.getAttribute("style").indexOf("display: none; ") >= 0) {
      item.setAttribute("style",
       item.getAttribute("style").replace("display: none; ", ""));
      title.setAttribute("style", "background-color: #73FF69;");
    }
    else {
      item.setAttribute("style",
       "display: none; ".concat(item.getAttribute("style")));
      title.setAttribute("style", "background-color: #FF7B74");
    }
  }
}

Expander.moveUp = function(root, item) {
  return function() {
    if (item.previousSibling)
      root.insertBefore(item, item.previousSibling);
  }
}

Expander.moveDown = function(root, item) {
  return function() {
    if (item.nextSibling)
      root.insertBefore(item, item.nextSibling.nextSibling);
  }
}

Expander.makeExpander = function(root) {
  var children = root.childNodes, items = [], item, i, expander, icon, title;
  var img1, img2;

  for (i = 0; i < children.length; i++)
    if (children[i].nodeType !== 3)
      items.push(item = children[i]);

  if (items.length > 0)
    root.setAttribute("style",
     "border: solid 2px; border-color: blue; width: 100%;");

  for (i = 0; i < items.length; i++) {
    items[i].setAttribute("style",
     "margin-left: 18px;".concat(items[i].getAttribute("style")));
    item = document.createElement("div")
    item.setAttribute("class", "item-block");

    expander = document.createElement("div");
    expander.setAttribute("class", "expander-block");

    img1 = document.createElement("img");
    img1.setAttribute("class", "icon");
    img1.setAttribute("src", "up.png");
    img2 = document.createElement("img");
    img2.setAttribute("class", "icon");
    img2.setAttribute("src", "down.png");

    title = document.createElement("div");
    title.setAttribute("class", "title-block");
    title.setAttribute("title", items[i].getAttribute("title"));
    title.innerHTML = items[i].getAttribute("title");
    title.addEventListener('click', Expander.showhide(items[i], title));

    // expander.appendChild(icon);
    expander.appendChild(img1);
    expander.appendChild(img2);
    expander.appendChild(title);

    root.insertBefore(item, items[i]);
    item.appendChild(expander);
    item.appendChild(items[i]);

    img1.addEventListener('click', Expander.moveUp(root, item));
    img2.addEventListener('click', Expander.moveDown(root, item));
  }

}
