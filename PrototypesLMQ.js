/*
Fill in the missing parts of this code so that the expression in the print is true. The intent is that you will use a shell session and experiment a bit, along with reasoning about the relationships between x.constructor, x.prototype, and x.__proto__
*/

var xCount;
var x = Function;
var y = Array.prototype;
var z = 0;
Object.prototype.mySpecialProperty = true;


for (xCount = 1; xCount < 100 && x == x.constructor; xCount++)
   x = x.constructor;

print("Result of five tests on prototypes and inheritance: " + (
 xCount == 100
 && y.__proto__.__proto__ == null
 && z.__proto__.__proto__ === Object.prototype
 && z.constructor.__proto__.__proto__ === Object.prototype
 && x.mySpecialProperty && y.mySpecialProperty && z.mySpecialProperty));

print((xCount == 100));
print((y.__proto__.__proto__ == null));
print((z.__proto__.__proto__ === Object.prototype));
print((z.constructor.__proto__.__proto__ === Object.prototype));
print((x.mySpecialProperty && y.mySpecialProperty && z.mySpecialProperty));
