/*
Problem:

Add the Math.fixTrig function so that the assignments below will alter the Math.sin, Math.cos, and Math.tan functions so that they take their arguments in degrees if a second parameter is supplied saying "d". See comments in main below. Add only the fixTrig property to Math. Use closure.
*/

Math.fixTrig = function(f) {

   return function(v) {
      return arguments[1]?f(v * Math.PI / 180):f(v);
   }
};

Math.sin = Math.fixTrig(Math.sin);
Math.cos = Math.fixTrig(Math.cos);
Math.tan = Math.fixTrig(Math.tan);

(function() {
   var angle;

   while (angle = parseFloat(readline())) {
      print(Math.sin(angle, "d").toPrecision(5));  // degrees
      print(Math.sin(angle).toPrecision(5));       // radians
      print(Math.cos(angle, "d").toPrecision(5));
      print(Math.cos(angle).toPrecision(5));
      print(Math.tan(angle, "d").toPrecision(5));
      print(Math.tan(angle).toPrecision(5));
   }
})();
