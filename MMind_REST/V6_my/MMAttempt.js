// Construct an MMAttempt, copying |prms| and |prsURI|, which are the game 
// parameters and URI of the player.
var MMAttempt = function(prms, prsUri) {
   this.prsUri = prsUri;
   this.attUri = null;
   
   this.gameParams = {
      ptnLength: prms.ptnLength,
      numLetters: prms.numLetters
   };
};

MMAttempt.mmaResource = "/Mmas";
MMAttempt.mmmResource = "/Moves";

// Start a new MMAttempt on the GMS server, on behalf of the player specified
// in the constructor.  If any error results, show a NoticeDlg.  Make the GMS
// call synchronous, so that the UI calling |init| must wait for the GMS 
// response.
MMAttempt.prototype.init = function() {
   this.moves = [];
   this.startTime = null;
   var self = this;
   
   this.startTime = new Date(),

   $.ajax({
      url: this.prsUri + MMAttempt.mmaResource,
      data: JSON.stringify(this.gameParams),
      async: false,
      contentType: "application/json",
      type: "POST",
 
      success: function(data, status, xhr) {
         self.attUri = xhr.getResponseHeader("Location");
      },
 
      error: function(xhr, status) {
         new NoticeDlg().run("Error starting new game -- is one in progress?");
      }
  });
};

// Accept |guess|, an array of 0-bsed numbers, as the next guess in the ongoing
// game.  Make the necessary GMS call, synchronously.  If there's a failure,
// show a NoticeDlg, otherwise return a three-property move object giving the
// original guess, and the number of exact and inexact matches.  Save this
// move object as well, in an array of objects called |moves|.
MMAttempt.prototype.match = function(guess) {
   var result;
   var self = this;
   
   $.ajax({
      url: this.attUri + MMAttempt.mmmResource,
 
      data: JSON.stringify({
         guess: guess
      }),

      async: false,
      contentType: "application/json",
      type: "POST",
 
      success: function(data, status, xhr) {
         data.guess = guess;
         self.moves.push(data);
      },
 
      error: function(xhr, status) {
         new NoticeDlg.run("Error posting a move");
      }
  });
  
  return this.moves[this.moves.length-1];
};

// Synchronously inform GMS that the player has quit the current game, posting
// a NoticeDlg in the event of error.  Accept an optional external MMAttempt URI,
// so that this method can be used to quit any ongoing game.
MMAttempt.prototype.quit = MMAttempt.quit = function(outsideUri) {
   $.ajax({
      url: outsideUri || this.attUri,
 
      async: false,
      type: "PUT",
 
      error: function(xhr, status) {
         new NoticeDlg().run("Error quitting a move");
      }
  });
}