var LoginDlg = function() {
   this.dlg = $('#loginDlg');
   this.login = $('#loginDlg #login');
   this.password = $('#loginDlg #serverUrl');
   this.errorList = $('#loginDlg #errorList');
}

LoginDlg.resource = "/Ssns";

LoginDlg.prototype.run = function(serverUrl, loginCB) {   
   var self = this;

   this.errorList.empty();
   this.loginCB = loginCB;
   this.serverUrl = serverUrl;
   
   this.dlg.dialog({
      width: 500,
      modal: true,
      buttons: [
         {text: "Ok", click: function() {self.ok();}},
         {text: "Cancel", click: function() {$(this).dialog("close");}} 
      ]
   });
}

LoginDlg.prototype.ok = function() {
   var self = this;
   
   this.errorList.empty();
   $.ajax({
      url: this.serverUrl + LoginDlg.resource,
 
      data: JSON.stringify({
         email: this.login.val(),
         password: this.password.val()
      }),

      contentType: "application/json",
      type: "POST",
 
      success: function(data, status, xhr) {
         self.loginSuccess(data, xhr);
      },
 
      error: function(xhr, status) {
         self.loginError(xhr, status);
      }
  });
}

LoginDlg.prototype.loginSuccess = function(data, packet) {
   var self = this;
   
   this.prsURI = packet.getResponseHeader("Location");
   
   $.ajax({
      url: this.prsURI,
 
      contentType: "application/json",
      type: "GET",
      dataType: "json",
 
      success: function(data) {
         self.getNameSuccess(data);
      },
 
      error: function(xhr, status) {
         self.getNameError(xhr, status);
      }
  });
}

LoginDlg.prototype.loginError = function(packet, status) {
   if (packet.status == 401)
      this.errorList.append
       ($('<li>That login and password are not in our records</li>'));
   else
      this.errorList.append($('<li>General error accessing server</li>'));
}

LoginDlg.prototype.getNameSuccess = function(data) {
   if (this.loginCB)
      this.loginCB(data.firstName, data.lastName, this.prsURI);
      
   this.dlg.dialog("close");
}

LoginDlg.prototype.getNameError = function(packet, status) {
   this.errorList.append($('<li>Error getting data for logged person</li>'));
}