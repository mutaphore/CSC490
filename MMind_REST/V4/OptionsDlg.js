var OptionsDlg = function() {
   this.dlg = $('#optionsDlg');
   this.ptnLength = $('#optionsDlg #ptnLength');
   this.serverUrl = $('#optionsDlg #serverUrl');
   this.errorList = $('#optionsDlg #errorList');
}

OptionsDlg.prototype.run = function(params) {   
   var self = this;

   // For most JQueryUI controls, all properties (e.g. checked) must be set before
   // calling the JQueryUI ornamenting function.
   this.params = params;
   
   $('#charChoices').buttonset();
   $('#charChoices #max' +
    String.fromCharCode(params.numLetters + 'A'.charCodeAt(0) - 1))
    .prop('checked', true);
   $('#charChoices').buttonset("refresh");
   
   this.ptnLength.val(params.ptnLength).spinner({
      max: Params.maxLen, min: Params.minLen
   });
   this.serverUrl.val(params.serverUrl);
   
   this.dlg.dialog({
      width: 450,
      modal: true,
      buttons: [
         {text: "Ok", click: function() {self.ok();}},
         {text: "Cancel", click: function() {$(this).dialog("close");}} 
      ]
   });
}

OptionsDlg.prototype.ok = function() {
   var newLength = parseInt(this.ptnLength.val());
   var errors = [];
   var selectedId;
   var self = this;
   
   if (newLength < 2 || newLength > Params.maxLen)
      errors.push("Number of chars must be between 2 and " + Params.maxLen);

   if (errors.length) {
      this.errorList.empty();
      errors.forEach(function(i) {self.errorList.append($('<li>' + i + '</li>'));});
   }
   else {
      this.params.ptnLength = newLength;
      this.params.serverUrl = this.serverUrl.val();
      $('#charChoices').children().each(function() {
         var choice = $(this);
         
         if (choice.prop('checked')) {
            selectedId = choice.attr('id').trim();
            self.params.numLetters = selectedId.charCodeAt(selectedId.length-1) - 'A'.charCodeAt(0) + 1;
         }
      });
      this.dlg.dialog("close");
   }
}