// Construct an MMAttempt with the given parameters.
var MMAttempt = function(prms) {
   this.gameParams = {
      ptnLength: prms.ptnLength,
      numLetters: prms.numLetters
   };
};

// Start the MMAttempt, including generating a model pattern conforming to the
// parameters from the constructor.  (Do this locally for now, though soon
// we'll do it by calling GMS.)
MMAttempt.prototype.init = function() {
   this.startTime = new Date();
   this.moves = [];
   this.content = [];
   for (var i = 0; i < this.gameParams.ptnLength; i++)
      this.content[i] = Math.floor(Math.random() * this.gameParams.numLetters);
};

// Take |g| as a guess against the current model, and return an object giving
// the guess itself, and the number of exact and inexact matches.  Again do
// this locally for now, but we'll soon do it via GMS calls.
MMAttempt.prototype.match = function(g) {
   var move = {
      guess: g,
      exact: 0,
      inexact: 0,
   };
   var thisUsed = [], otherUsed = [];

   for (var i = 0; i < this.content.length; i++)
      if (this.content[i] === move.guess[i]) {
         thisUsed[i] = otherUsed[i] = true;
      move.exact++;
   }
   
   for (var i = 0; i < this.content.length; i++)
      for (var j = 0; j < move.guess.length; j++)
         if (!thisUsed[i] && !otherUsed[j]
          && this.content[i] === move.guess[j]) {
            thisUsed[i] = otherUsed[j] = true;
            move.inexact++;
         }
     
   this.moves.push(move);     
   return move;
}