// Initialize a Guess with the content specified, or with an
// empty array if no content is given.  Hold the content as
// an array of 0-based values, not as letters.  This imitates
// the GMS representation of guesses.
var Guess = function(content) {
   this.content = content || [];
}
   
// Return a string representation of the guess, as a series of letters for
// UI display (not for passing to GMS)
Guess.prototype.toString = function() {
   var rtn = '';
      
   for (var i = 0; i < this.content.length; i++) {
      rtn = rtn + String.fromCharCode('A'.charCodeAt(0) + this.content[i]);
      if (i < this.content.length-1)
         rtn = rtn + ' ';
   }
   return rtn;
};

// Parse |line| into a guess.  Assume |line| is a string of letters, but
// parse them into an array of 0-based numbers, per GMS notation.  Use |params|
// for the size of guess and permitted range of letters.  Return a possibly
// empty array of error strings for any errors encountered in the parse.
Guess.prototype.parse = function(line, params) {
   var line, letters = [], letter, errors = [];

   letters = line.trim().toUpperCase().split('');
   for (var i = 0; i < letters.length; i++) {
      letter = letters[i].charCodeAt(0) - 'A'.charCodeAt(0);
      if (letter < 0 || letter >= params.numLetters)
         errors.push(letters[i] + " is not a valid letter");
      letters[i] = letter;  // save numerical version instead.
   }
   if (letters.length < params.ptnLength)
      errors.push("Guess of " + letters.length + " is too short");
         
   else if (letters.length > params.ptnLength)
      errors.push("Guess is too long");
      
   if (!errors.length) {
      this.content = [];
      for (var i = 0; i < letters.length; i++)
         this.content[i] = letters[i]
   }
      
   return errors;
};