// Construct an MMAttempt, copying |prms| and |prsURI|, which are the game
// parameters and URI of the player.
var MMAttempt = function(prms, prsURI) {
   this.gameParams = {
      ptnLength: prms.ptnLength,
      numLetters: prms.numLetters
   };
   this.prsURI = prsURI;
   this.moves = [];
};


// Start a new MMAttempt on the GMS server, on behalf of the player specified
// in the constructor. Note the starting time, for later display purposes.
// If any error results, show a NoticeDlg. Make the GMS
// call synchronous, so that the UI that is calling |init| must wait for the GMS
// response.
MMAttempt.prototype.init = function() {
   var errors = [];
   var self = this;

   this.startTime = new Date();
   
   $.ajax({
      url: this.prsURI + "/Mmas";
      type: "POST",
      contentType: "application/json",
      async: false,
      data: JSON.stringify(this.gameParams),
      success: function(data, status, xhr) {
         self.attUri = xhr.getReponseHeader("Location");
      },
      failure: function(xhr, status) {
         var errorDlg = new NoticeDlg();
         var errors = JSON.parse(packet.responseText);
         errorDlg.run(errors.toString());
      }
   });

};

// Accept |guess|, an array of 0-based numbers, as the next guess in the ongoing
// game. Make the necessary GMS call, synchronously. If there's a failure,
// show a NoticeDlg, otherwise return a three-property move object giving the
// original guess, and the number of exact and inexact matches. Save this
// move object as well, in an array of objects called |moves|.
MMAttempt.prototype.match = function(g) {
   var self = this;
   var move = null;

   $.ajax({
      url: self.attUri + "/Moves",
      contentType: "application/json",
      async: false,
      data: JSON.stringify({guess: g}),
      success: function(data, status, xhr) {
         move = data;
         self.moves.push(move);     
      },
      error: function(xhr, status) {
         new NoticeDlg.run("Cannot make move");
      }
   });

   return move;
}

// Synchronously inform GMS that the player has quit the current game, posting
// a NoticeDlg in the event of error. Accept an optional external MMAttempt URI,
// so that this method can be used to quit any ongoing game, not just the one
// represented by the MMAttempt.
MMAttempt.prototype.quit = MMAttempt.quit = function(outsideUri) {
   
   var self = this;

   $.ajax({
      url: self.attUri,
      contentType: "application/json",
      type: "PUT",
      async: false,
      error: function(packet, status) {
         var errorDlg = new NoticeDlg();
         var errors = JSON.parse(packet.responseText);
         errorDlg.run(errors.toString());
      }
   })
}
