var NoticeDlg = function() {
   this.dlg = $('#noticeDlg');
}

NoticeDlg.prototype.run = function(notice) {   
   this.dlg.html(notice);
   this.dlg.dialog({
      modal: true,
      buttons: [
         {text: "Ok", click: function() {$(this).dialog("close");}} 
      ]
   });
}