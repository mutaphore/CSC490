var RegisterDlg = function() {
   this.dlg = $('#registerDlg');
   this.email = $('#registerDlg #email');
   this.firstName = $('#registerDlg #firstName');
   this.lastName = $('#registerDlg #lastName');
   this.password = $('#registerDlg #password');
   this.passwordCnf = $('#registerDlg #passwordCnf');
   this.errorList = $('#registerDlg #errorList');
}

RegisterDlg.resource = "/Prss";

RegisterDlg.prototype.run = function(serverUrl) {   
   var self = this;

   this.serverUrl = serverUrl;
   this.dlg.dialog({
      width: 500,
      modal: true,
      buttons: [
         {text: "Ok", click: function() {self.ok();}},
         {text: "Cancel", click: function() {$(this).dialog("close");}} 
      ]
   });
}

RegisterDlg.prototype.ok = function() {
   var errors = [];
   var self = this;
   var password = this.password.val(), confirm = this.passwordCnf.val();
   
   if (!password || !confirm)
      errors.push("Password and confirmation are both required.");
   else if (password.valueOf() !== confirm.valueOf())
      errors.push("Password and confirmation do not match");
      
   if (errors.length) {
      this.errorList.empty();
      this.password.val("");
      this.passwordCnf.val("");
      errors.forEach(function(err) {
         self.errorList.append($('<li>' + err + '</li>'));
      });
   }
   else {
      $.ajax({
         url: this.serverUrl + RegisterDlg.resource,
    
         data: JSON.stringify({
            email: this.email.val(),
            firstName: this.firstName.val(),
            lastName: this.lastName.val(),
            password: this.password.val()
         }),

         contentType: "application/json",
         type: "POST",
    
         success: function(data, status, xhr) {
            self.registerSuccess(data, xhr);
         },
    
         error: function(xhr, status) {
            self.registerError(xhr, status);
         }
     });
   }
}

RegisterDlg.prototype.registerSuccess = function(data, packet) {
   var prsURI = packet.getResponseHeader("Location");
   
   console.log("Register success for: " + prsURI);
   this.dlg.dialog("close");
}

RegisterDlg.prototype.registerError = function(packet, status) {
   var errors = JSON.parse(packet.responseText);
   var self = this;

   this.errorList.empty();
   errors.forEach(function(err) {
      self.errorList.append($('<li>' + err + '</li>'));
   });
}