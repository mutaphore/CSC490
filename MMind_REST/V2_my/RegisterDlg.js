var RegisterDlg = function() {
	this.dlg = $("#registerDlg");
	this.email = $("#email");
	this.firstName = $("#firstName");
	this.lastName = $("#lastName");
	this.password = $("#password");
	this.confPassword = $("#confPassword");
	this.errorList = $("#regErrorList");
	this.url = "";
}

RegisterDlg.prototype.run = function(url) {
	var self = this;

	this.url = url;
	this.dlg.dialog({
		width: "500px",
		buttons:[ 
			{text: "Ok", click: function(){self.configure()}},
			{text: "Cancel", click: function(){$(this).dialog("close")}}
		]
	});
}

RegisterDlg.prototype.configure = function() {
	var self = this;
	var errors = [];

	this.errorList.empty();
	if (this.password.val() !== this.confPassword.val())
		errors.push("Password and confirmation do not match");
	if (!this.firstName.val() || !this.lastName.val())
		errors.push("Need first and last names");
	if (!this.email.val())
		errors.push("No email entered");

	if (errors.length)
		errors.forEach(function(e) {self.errorList.append($("<li>" + e + "</li>"))});
	else {
		$.ajax({
			url: self.url + 'Prss',
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify({
				email: self.email.val(),
				firstName: self.firstName.val(),
				lastName: self.lastName.val(),
				password: self.password.val()
			}),
			success: function(data, status, xhr) {
				self.dlg.dialog("close");
				console.log(xhr.getResponseHeader("Location"));
			},
			error: function() {self.errorList.append($("<li>Cannot Register!</li>"));}
		});
	}
}
