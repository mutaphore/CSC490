var NoticeDlg = function() {
	this.dlg = $("#noticeDlg");
	this.message = $("#message");
}

NoticeDlg.prototype.run = function(msg) {
	this.message.text(msg);
	this.dlg.dialog({
		width: "250px",
		buttons:[
			{text: "OK", click: function(){$(this).dialog("close")}},
		]
	});
}