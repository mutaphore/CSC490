var Pattern = function(params) {
   this.params = params;
   this.content = [];
   
   for (var i = 0; i < params.length; i++)
      this.content[i] = 0;
   
   this.toString = function() {
      var rtn = this.content[0];
      
      for (var i = 1; i < this.content.length; i++)
        rtn = rtn + " " + this.content[i];
      return rtn;
   };
   
   this.parse = function(line) {   
      var line, letters = [], letter, errors = [];

      letters = line.trim().toUpperCase().split('');
      for (var i = 0; i < letters.length; i++) {
         letter = letters[i];
         if (letter < 'A' || letter > this.params.maxChar)
            errors.push(letters[i] + " is not a valid letter");
      }
      if (letters.length < this.params.length)
         errors.push("Guess of " + letters.length + " is too short");
         
      else if (letters.length > this.params.length)
         errors.push("Guess is too long");
      
      if (!errors.length)
         for (var i = 0; i < letters.length; i++)
            this.content[i] = letters[i].trim();
      
      return errors;
   };
   
   this.init = function(s) {
      var randRange = this.params.maxChar.charCodeAt(0) - 'A'.charCodeAt(0) + 1;

      for (var i = 0; i < this.params.length; i++) {
         this.content[i] = String.fromCharCode('A'.charCodeAt(0)
          + Math.random() * randRange);
      }
   }
   
   this.match = function(other) {
      var result = {
         exact: 0,
         inexact: 0,
         missing: 0,
         toString: function() {
            return this.exact + ' exact and ' + this.inexact + ' inexact.';
         },
         imperfect: function() {return this.missing > 0;}
      };
      var thisUsed = [], otherUsed = [];
      
      for (var i = 0; i < this.content.length; i++)
         if (this.content[i] === other.content[i]) {
            thisUsed[i] = otherUsed[i] = true;
         result.exact++;
      }
   
      for (var i = 0; i < this.content.length; i++)
         for (var j = 0; j < other.content.length; j++)
            if (!thisUsed[i] && !otherUsed[j]
             && this.content[i] === other.content[j]) {
               thisUsed[i] = otherUsed[j] = true;
              result.inexact++;
            }
            
      result.missing = this.params.length - result.exact;
      return result;
   }
};