  var MMind = function() {
   this.attempt = null;       // no current attempt
   
   this.gameTime = 0;
   this.timer = null;

   this.params = new Params(); // default parameters
   this.optionsDlg = new OptionsDlg();
   this.loginDlg = new LoginDlg();
   this.noticeDlg = new NoticeDlg();
   this.registerDlg = new RegisterDlg();
   
   this.gameBtn = $('#gameBtn').button();
   this.moveBtn = $('#moveBtn').button();
};

MMind.makeMoveHTML = function(move) {
   return $('<li>' + new Guess(move.guess).toString()
    + '<span class="inexact">' + move.inexact + '</span>'
    + '<span class="exact">' + move.exact + '</span></li>');
};

MMind.prototype.configure = function() {
   var self = this;
   
   $('#history').accordion({collapsible: true});
   
   this.gameBtn.button("disable").on('click', function(e) {self.startGame();});
   this.moveBtn.button().button("disable").on('click', function(e) {self.doMove();});
   
   $('#userMenu').menu({
      position: {my: "left top", at: "left bottom"},
      icons: {submenu: "ui-icon-circle-triangle-s"},
      select: function(event, ui) {
         var dlg = ui.item.attr('dlg');
         
         if (dlg === 'OptionsDlg')
            self.optionsDlg.run(self.params);
         else if (dlg == 'RegisterDlg')
            self.registerDlg.run(self.params.serverUrl);
         else if (dlg == 'LoginDlg')
            self.loginDlg.run(self.params.serverUrl, self.login.bind(self));
      }
   }).tooltip({
      position: {my: "left bottom", at: "left+15 top"}
   });
   
   $('#langMenu').menu({
      position: {my: "left top", at: "left bottom"},
      icons: {submenu: "ui-icon-circle-triangle-s"},
      select: function(event, ui) {
         if (ui.item.attr('cc'))
            self.noticeDlg.run("Switching to " + ui.item.attr('cc'));
      }
   }).tooltip({
      position: {my: "left bottom", at: "left+15 top"}
   });
};

MMind.prototype.startGame = function() {
   var self = this;
   
   this.attempt = new MMAttempt(this.params, this.loggedUri);
   this.attempt.init();
   
   $('#gameMoves').empty().append
    ($("<li><input id='nextMove' type='text'></input></li>"));
   $('#playArea #errorList').empty();
   
   $('#nextMove').attr('maxLength', this.params.ptnLength);

   this.gameBtn.button('option', 'label', 'Quit Game');
   this.gameBtn.unbind('click');
   this.gameBtn.click(function() {self.attempt.quit(); self.stopGame();});

   this.moveBtn.button('enable');
   
   this.gameTime = 0;
   $('#statusBar #timer').text('Game Duration 0s');
   this.timer = setInterval(function() {
      self.gameTime++;
      $('#statusBar #timer').text('Game Duration ' + self.gameTime + 's');
   }, 1000);
}

MMind.prototype.doMove = function() {
   var moveNode = $('#nextMove');
   var errorNode = $('#playArea #errorList');
   var guess = new Guess(this.params);
   var errors, move, newEntry;
   var self = this;

   errors = guess.parse(moveNode.val(), this.params);
   errorNode.empty();
   if (errors.length)
      errors.forEach(function(i) {errorNode.append($('<li>' + i + '</li>'));});
   else {
      move = this.attempt.match(guess.content);
      MMind.makeMoveHTML(move).insertBefore(moveNode.parent());
      
      if (move.exact === guess.content.length) {
         errorNode.append($('<li>Correct!</li>'));
         this.stopGame();
         this.attempt.duration = this.gameTime;
         this.postGame(this.attempt);
      }
   }
}

MMind.prototype.stopGame = function() {
   var self = this;
   
   this.gameBtn.button('option', 'label', 'Start Game');
   
   this.gameBtn.unbind('click');   // Important!
   this.gameBtn.click(function() {self.startGame();});
   this.moveBtn.button('disable');
   clearInterval(this.timer);
}

MMind.prototype.postGame = function(attempt) {
   var history = $('#history');
   var moves = attempt.moves;
   var summary = moves.length + ' moves in ' + attempt.duration
    + 's on ' + attempt.startTime.toLocaleString();
   var moveList = $('<ol></ol>');
   var answer = new Guess(moves[moves.length-1].guess);
   
   moves.forEach(function(m) {
      moveList.append(MMind.makeMoveHTML(m));
   });
   
   history.append($('<h3 class="historyEntry">' + summary + '</h3>'))
    .append($('<div class="historyExpansion"></div>')
    .append($('<h3>Actual pattern: ' + answer.toString() + '</h3>'))
    .append(moveList));
    
   history.accordion("refresh");
}

MMind.mmaResource = "/Mmas";

MMind.prototype.login = function(first, last, uri) {
   var self = this;
   
   $('#statusBar #login').text('Logged user: ' + first + ' ' + last);
   this.loggedUri = uri;
   if (this.timer)
      this.stopGame();
   else
      this.gameBtn.button("enable");
   
   $.ajax({
      url: uri + MMind.mmaResource,
 
      contentType: "application/json",
      type: "GET",
      dataType: "json",
 
      success: function(data) {
         self.getMmaSuccess(data);
      },
 
      error: function(xhr, status) {
         self.getMmaError(xhr, status);
      }
   });
};

MMind.prototype.getMmaSuccess = function(data) {
   var self = this;
   
   $('#history').empty().accordion("refresh");
   data.forEach(function(mma) {
      if (mma.score >= 0) {
         mma.startTime = new Date(mma.startTime);
         self.postGame(mma);
      }
   });
   
   if (data.length && data[data.length-1].duration < 0)
      MMAttempt.quit(this.params.serverUrl + '/' + data[data.length-1].uri);
};

MMind.prototype.getMmaError = function(xhr, status) {
   this.noticeDlg.run("Error getting existing game information from server.  Please retry login.");
};