var MMind = function() {
   this.model = null;          // no current game
   this.moves = [];            // no move history
   
   this.gameTime = 0;
   this.timer = null;

   this.params = new Params(); // default parameters
   this.optionsDlg = new OptionsDlg();
   this.loginDlg = new LoginDlg();
   this.noticeDlg = new NoticeDlg();
   this.registerDlg = new RegisterDlg();
   
   this.gameBtn = $('#gameBtn').button();
   this.moveBtn = $('#moveBtn').button();
};

MMind.makeMoveHTML = function(move, matches) {
   return $('<li>' + move.toString()
    + '<span class="inexact">' + matches.inexact + '</span>'
    + '<span class="exact">' + matches.exact + '</span></li>');
};

MMind.prototype.configure = function() {
   var self = this;
   
   $('#history').accordion({collapsible: true});
   this.gameBtn.button("disable").on('click', function(e) {self.startGame();});
   this.moveBtn.button().button("disable").on('click', function(e) {self.doMove();});
   
   $('#userMenu').menu({
      position: {my: "left top", at: "left bottom"},
      icons: {submenu: "ui-icon-circle-triangle-s"},
      select: function(event, ui) {
         var dlg = ui.item.attr('dlg');
         
         if (dlg === 'OptionsDlg')
            self.optionsDlg.run(self.params);
         else if (dlg == 'RegisterDlg')
            self.registerDlg.run(self.params.serverUrl);
         else if (dlg == 'LoginDlg')
            self.loginDlg.run(self.params.serverUrl, self.login);
      }
   }).tooltip({
      position: {my: "left bottom", at: "left+15 top"}
   });
   
   $('#langMenu').menu({
      position: {my: "left top", at: "left bottom"},
      icons: {submenu: "ui-icon-circle-triangle-s"},
      select: function(event, ui) {
         if (ui.item.attr('cc'))
            self.noticeDlg.run("Switching to " + ui.item.attr('cc'));
      }
   }).tooltip({
      position: {my: "left bottom", at: "left+15 top"}
   });
};

MMind.prototype.startGame = function() {
   var self = this;
   
   this.model = new Pattern(this.params);
   this.model.init();
   
   $('#gameMoves').empty().append
    ($("<li><input id='nextMove' type='text'></input></li>"));
   this.moves = [];
   $('#playArea #errorList').empty();
   
   $('#nextMove').attr('maxLength', this.params.length);

   this.gameBtn.button('option', 'label', 'Quit Game');
   this.gameBtn.unbind('click');
   this.gameBtn.click(function() {self.stopGame();});

   this.moveBtn.button('enable');
   
   this.gameTime = 0;
   $('#statusBar #timer').text('Game Duration 0s');
   this.timer = setInterval(function() {
      self.gameTime++;
      $('#statusBar #timer').text('Game Duration ' + self.gameTime + 's');
   }, 1000);
}

MMind.prototype.doMove = function() {
   var moveNode = $('#nextMove');
   var errorNode = $('#playArea #errorList');
   var guess = new Pattern(this.params);
   var errors, matches, newEntry;
   var self = this;

   errors = guess.parse(moveNode.val());
   errorNode.empty();
   if (errors.length)
      errors.forEach(function(i) {errorNode.append($('<li>' + i + '</li>'));});
   else {
      matches = this.model.match(guess);
      this.moves.push({move: guess, matches: matches});
      MMind.makeMoveHTML(guess, matches).insertBefore(moveNode.parent());
      
      if (matches.exact === this.model.content.length) {
         errorNode.append($('<li>Correct!</li>'));
         this.stopGame();
         this.postGame(this.model, this.moves);
      }
   }
}

MMind.prototype.stopGame = function() {
   var self = this;
   
   this.gameBtn.button('option', 'label', 'Start Game');
   
   this.gameBtn.unbind('click');   // Important!
   this.gameBtn.click(function() {self.startGame();});
   this.moveBtn.button('disable');
   clearInterval(this.timer);
}

MMind.prototype.postGame = function(model, moves) {
   var history = $('#history');
   var summary = moves.length + ' moves in ' + this.gameTime
    + 's on ' + new Date().toDateString();
   var moveList = $('<ol></ol>');
   
   moves.forEach(function(m) {
      moveList.append(MMind.makeMoveHTML(m.move, m.matches));
   });
   
   history.append($('<h3 class="historyEntry">' + summary + '</h3>'))
    .append($('<div class="historyExpansion"></div>')
    .append($('<h3>Actual pattern: ' + model.toString() + '</h3>'))
    .append(moveList));
    
   history.accordion("refresh");
}

MMind.prototype.login = function(first, last, uri) {
   var self = this;
   
   $('#statusBar #login').text('Logged user: ' + first + ' ' + last);
   this.loggedURI = uri;
   this.gameBtn.button("enable");
};