var OptionsDlg = function() {
   this.dlg = $('#optionsDlg');
   this.ptnLength = $('#optionsDlg #ptnLength');
   this.serverUrl = $('#optionsDlg #serverUrl');
   this.errorList = $('#optionsDlg #errorList');
}

OptionsDlg.prototype.run = function(params) {   
   var self = this;

   // For most JQueryUI controls, all properties (e.g. checked) must be set before
   // calling the JQueryUI ornamenting function.
   this.params = params;
   
   $('#charChoices').buttonset();
   $('#charChoices #max'+params.maxChar).click(); //or prop('checked', true)?;
   
   this.ptnLength.val(params.length).spinner(
      {max: Params.maxLen, min: Params.minLen}
   );
   this.serverUrl.val(params.serverUrl);
   
   this.dlg.dialog({
      width: 450,
      modal: true,
      buttons: [
         {text: "Ok", click: function() {self.ok();}},
         {text: "Cancel", click: function() {$(this).dialog("close");}} 
      ]
   });
}

OptionsDlg.prototype.ok = function() {
   var newLength = parseInt(this.ptnLength.val());
   var errors = [];
   var selectedId;
   var self = this;
   
   if (newLength < 2 || newLength > Params.maxLen)
      errors.push("Number of chars must be between 2 and " + Params.maxLen);

   this.errorList.empty();
   if (errors.length) {
      errors.forEach(function(i) {self.errorList.append($('<li>' + i + '</li>'));});
   }
   else {
      this.params.length = newLength;
      this.params.serverUrl = this.serverUrl.val();
      $('#charChoices').children().each(function() {
         var choice = $(this);
         
         if (choice.prop('checked')) {
            selectedId = choice.attr('id').trim();
            self.params.maxChar = selectedId.charAt(selectedId.length-1);
         }
      });
      this.dlg.dialog("close");
   }
}