var LoginDlg = function() {
	this.dlg = $("#loginDlg");
	this.login = $("#loginDlg #login");
	this.password = $("#loginPassword");
	this.errorList = $("#loginDlg #errorList");
	this.serverUrl = "";
	this.prssURI = "";
}

LoginDlg.prototype.run = function(serverUrl) {
	var self = this;

	this.serverUrl = serverUrl;       
	this.dlg.dialog({
		width: 500,
		modal: true,
		buttons: [
			{text: "Ok", click: function() {self.ok();}},
			{text: "Cancel", click: function() {$(this).dialog("close");}}
		]
	});
}

LoginDlg.prototype.ok = function() {
	var errors = [];
	var self = this;

	console.log(this.serverUrl + "/Ssns");
	$.ajax({
		url: this.serverUrl + "/Ssns",
		data: JSON.stringify({
			email: this.login.val(),
			password: this.password.val()
		}),
		contentType: "application/json",
		type: "POST",
		success: function(data, status, xhr) {
			self.loginSuccess(xhr);
		},
		error: function(xhr, status) {
			self.loginError(xhr, status);
		}
	});
}

LoginDlg.prototype.loginSuccess = function(packet) {
	var self = this;

	this.prssURI = packet.getResponseHeader("Location");
	console.log("Got response: " + this.prssURI);

	$.ajax({
		url: this.prssURI,
		type: "GET",
		dataType: "json",
		success: function(data, status, xhr) {
			$("#statusBar #login").empty();
			$("#statusBar #login").append("Logged User: " + data.firstName + " " + data.lastName);
		},
		error: function(xhr, status) {
			self.loginError(xhr, status);
		}
	});
}

LoginDlg.prototype.loginError = function(xhr, status) {
	
	console.log("Status: " + status);
	this.errorList.empty();

	if (xhr.status == 401)
		this.errorList.append("<li>That login and password are not in our records.</li>");
	else
		this.errorList.append("<li>General server error.</li>");
}