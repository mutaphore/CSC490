var MMCountAttempts = function() {
   this.email = $('#email');
   this.url = $('#url');
   this.password = $('#password');
   this.showBtn = $('#showBtn');
   this.msgList = $('#msgList');
};

MMCountAttempts.prototype.configure = function() {
   var self = this;
   
   this.showBtn.click(function() {
      $.ajax({
         url: self.url.val() + '/Ssns',
         type: "POST",
         contentType: "application/json",
 
         data: JSON.stringify({
            email: self.email.val(),
            password: self.password.val()
         }),
 
         success: function(data, status, xhr) {
            self.loginSuccess(data, xhr);
         },
 
         error: function(packet, status) {
            self.loginError(packet, status);
         }
      });
   });
};

MMCountAttempts.prototype.loginSuccess = function(data, packet) {
   var url = packet.getResponseHeader('Location');
   var self = this;
   
   this.msgList.empty();
   this.msgList.append($('<li>Logged in as URL ' + url + '</li>'));
    
   $.ajax({
      url: url + '/Mmas',
      type: "GET",

      success: function(data, status, packet) {
         self.msgList.append($('<li>You have ' + data.length + ' attempts</li>'));
      },

      error: function(packet, status) {
         self.msgList.append($('<li>General error getting attempts</li>'));
      }
   });
}

MMCountAttempts.prototype.loginError = function(packet, status) {
   this.msgList.empty();
   this.msgList.append($('<li>Login error</li>'));
};