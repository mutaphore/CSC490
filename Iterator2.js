load("LinkedList4.js");

LinkedList.prototype.iterator = function() {
   var curNode = this.head;
   var that = this;

   return {
      hasNext: function() {return curNode;},
      advance: function() {curNode = curNode && curNode.next;},
      current: function() {return curNode && curNode.data;},
      reset: function() {curNode = that.head;}
   }
};

(function() {
   var val, itr, list = new LinkedList();

   [42, 14, 100].forEach(function(val) {list.add(val);});
   for (itr = list.iterator(); itr.hasNext(); itr.advance())
      print(itr.current());

   itr.reset();
   print(itr.current()); // undefined.. ?
})();
