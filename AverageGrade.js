//Fill in the code for averageGrade so that it reads a series of lines comprising gpa values, separated by commas, and for each such line, reports the average gpa for that line. End if you encounter a line that is empty or has only blanks. If any GPA figure is not a valid float or is missing, treat it as though it were 0.0.

var averageGrade = function() {
   var ln, grade, total;

   while (ln = readline()) {
      if (ln = ln.trim()) {
         grade = ln.split(",")
         total = 0
         for (var i = 0; i < grade.length; i++) {
            if (parseFloat(grade[i]))
               total += parseFloat(grade[i])
         }
         print("Average GPA: " + (total/grade.length).toFixed(2))
      }
   }
};

averageGrade();
