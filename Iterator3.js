load("LinkedList4.js");

LinkedList.prototype.iterator = function() {
   
   return {
      curNode: this.head,
      hasNext: function() {return this.curNode;},
      advance: function() {this.curNode = this.curNode && this.curNode.next;},
      current: function() {return this.curNode && this.curNode.data;},
      reset: function() {this.curNode = that.head;}
   };
};

(function() {
   var val, itr, list = new LinkedList();

   [42, 14, 100].forEach(function(val) {list.add(val);});
   for (itr = list.iterator(); itr.hasNext(); itr.advance())
      print(itr.current());

   itr.reset();
   print(itr.current()); // undefined.. ?
})();
