window.onload = function() {
   var tabControls = document.getElementsByClassName('tab-block');
   var i, j, tabs, pages, tabControl;
   
   for (i = 0; i < tabControls.length; i++) {
   
      tabControl = tabControls[i];
      tabs = tabControl.getElementsByClassName('tab');
      pages = tabControl.getElementsByClassName('page');

      tabs[0].setAttribute('class', 'tab selected-tab');
      pages[0].setAttribute('class', 'page selected-page');
      tabControl.selectedTab = tabs[0];
      tabControl.selectedPage = pages[0];

      for (j = 0; j < tabs.length; j++) {
         tabs[j].matchPage = pages[j];
         tabs[j].control = tabControl;

         tabs[j].addEventListener('click', function() {
            var control = this.control;
            
            control.selectedTab.setAttribute('class', 'tab');
            control.selectedPage.setAttribute('class', 'page');
            this.setAttribute('class', 'tab selected-tab');
            this.matchPage.setAttribute('class', 'page selected-page');
            control.selectedTab = this;
            control.selectedPage = this.matchPage;
         });
      }
   }
};