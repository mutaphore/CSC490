var MMind = function() {
   this.model = null;          // no current game
   this.moves = [];            // no move history
   this.params = new Params(); // default parameters
   this.optionsDlg = new OptionsDlg();
};

MMind.makeMoveHTML = function(move, matches) {
   return $('<li>' + move.toString()
    + '<span class="inexact">' + matches.inexact + '</span>'
    + '<span class="exact">' + matches.exact + '</span></li>');
};

MMind.prototype.configure = function() {
   var self = this;
   
   $('#history').accordion({collapsible: true,
    animate: {duration: 1000, easing: "easeInOutElastic"}});
   $('#optionsBtn').button().tooltip().on('click', function(e) {
      self.optionsDlg.run(self.params);
   });

   $('#gameBtn').button().on('click', function(e) {self.startGame();});
   $('#moveBtn').button().on('click', function(e) {self.doMove();});
};

MMind.prototype.startGame = function() {
   var self = this;
   var gameBtn = $('#gameBtn');
   
   this.model = new Pattern(this.params);
   this.model.init();
   if (this.params.testMode)
      $('#gameHeader').text("Current Game -- " + this.model.toString())
   
   $('#gameMoves').empty().append
    ($("<li><input id='nextMove' type='text'></input></li>"));
   this.moves = [];
   $('#playArea #errorList').empty();

   $('#nextMove').attr('maxLength', this.params.length);
   gameBtn.button('option', 'label', 'Quit Game');

   gameBtn.unbind('click');
   gameBtn.click(function() {self.stopGame();});

   this.gameTime = 0;
   $('#timer').text('Game Duration 0s');
   this.timer = setInterval(function() {
      self.gameTime++;
      $('#timer').text('Game Duration ' + self.gameTime + 's');
   }, 1000);
}

MMind.prototype.doMove = function() {
   var moveNode = $('#nextMove');
   var errorNode = $('#playArea #errorList');
   var gameBtn = $('#gameBtn');
   var guess = new Pattern(this.params);
   var errors, matches, newEntry;
   var self = this;

   errors = guess.parse(moveNode.val());
   errorNode.empty();
   if (errors.length)
      errors.forEach(function(i) {errorNode.append($('<li>' + i + '</li>'));});
   else {
      matches = this.model.match(guess);
      this.moves.push({move: guess, matches: matches});
      MMind.makeMoveHTML(guess, matches).insertBefore(moveNode.parent());
      
      if (matches.exact === this.model.content.length) {
         errorNode.append($('<li>Correct!</li>'));
         this.stopGame();
         this.postGame(this.model, this.moves);
      }
   }
}

MMind.prototype.stopGame = function() {
   var gameBtn = $('#gameBtn');
   var self = this;
   
   $('#gameHeader').text('Current Game');
   gameBtn.button('option', 'label', 'Start Game');
   
   gameBtn.unbind('click');
   gameBtn.click(function() {self.startGame();});
   clearInterval(this.timer);
}

MMind.prototype.postGame = function(model, moves) {
   var history = $('#history');
   var summary = moves.length + ' moves in ' + this.gameTime
    + 's on ' + new Date().toDateString();
   var moveList = $('<ol></ol>');
  
   moves.forEach(function(m) {
      moveList.append(MMind.makeMoveHTML(m.move, m.matches));
   });

   history.append($('<h3 class="historyEntry">' + summary + '</h3>'))
    .append($('<div class="historyExpansion"></div>')
    .append($('<h3>Actual pattern: ' + model.toString() + '</h3>'))
    .append(moveList));
    
   history.accordion("refresh");
}