Math.seed = 0;

Math.random = function(s) {
   if (s) {
      Math.seed = parseInt(s);
      return;
   }
      
   Math.seed = (Math.seed * 131071 + 524287) % 8191;
   return Math.seed/8192;
};