Math.random = (function() {
   var s;

   return function() {
      if (arguments[0])
         s = arguments[0];
      else {
         s = (s * 131071 + 524287) % 8191;
         return s / 8192;
      }
   }
})();

(function() {
   var rnd;

   for (var i = 0; i < 2; i++) {
      Math.random(1);  // Set seed to 1
      do {
         print(rnd = Math.random());
      } while (rnd < .90);
   }
})();
