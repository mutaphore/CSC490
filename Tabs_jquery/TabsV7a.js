var TabsV7 = {};

TabsV7.makeDiv = function(classAttr) {
   return $('<div></div>').addClass(classAttr);
}

TabsV7.tabify = function(root) {
   var tab, tabs = [], children = root.children(); 
   var tabBlock, rowDiv, page, margin, p;
   
   tabBlock = TabsV7.makeDiv('tab-block').append(root.children()).appendTo(root);
   rowDiv = TabsV7.makeDiv('row back-row').prependTo(tabBlock);
   
   margin = rowDiv.outerWidth();
   for (p = 0; p < pages.length; p++) {
      pages[p].addClass('page');
      tabs.push(tab = TabsV7.makeDiv('tab'));
      tab.append(pages[p].attr('title') || "Page " + (p+1));
      rowDiv.append(tab);  // Must parent the tab to get a width

      margin -= tab.outerWidth();
      if (margin < 0) {
         rowDiv = TabsV7.makeDiv('row back-row').insertBefore(pages[0]);
         rowDiv.append(tab);
         margin = rowDiv.outerWidth() - tab.outerWidth();  
      }
   }
   rowDiv.removeClass('back-row');
   
   tabs[0].addClass('tab selected-tab');
   pages[0].addClass('page selected-page');
   tabBlock.selectedTab = tabs[0];
   tabBlock.selectedPage = pages[0];

   for (j = 0; j < tabs.length; j++) {
      tabs[j].matchPage = pages[j];
      tabs[j].control = tabBlock;
      tabs[j][0].jq = tabs[j];
      tabs[j].bind('click', function() {
         var $this = this.jq;    // $(this) doesn't work
         var control = $this.control;
            
         control.selectedTab.removeClass('selected-tab');
         control.selectedPage.removeClass('selected-page');
         $this.addClass('selected-tab');
         $this.matchPage.addClass('selected-page');
         control.selectedTab = $this;
         control.selectedPage = $this.matchPage;
      });
   }
};

$(function() {
   TabsV7.tabify($('#block1'));
});