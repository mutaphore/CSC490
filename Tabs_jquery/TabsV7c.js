var TabsV7 = {};

TabsV7.makeDiv = function(classAttr) {
   return $('<div></div>').addClass(classAttr);
}

TabsV7.tabify = function(root) {
   var tab, tabs = [], children = root.children(); 
   var tabBlock, rowDiv, page, margin, p;
   
   tabBlock = TabsV7.makeDiv('tab-block').append(root.children()).appendTo(root);
   rowDiv = TabsV7.makeDiv('row back-row').prependTo(tabBlock);
   
   margin = rowDiv.outerWidth();
   children.each(function(p){
      var page = $(this).addClass('page');
      var tab = TabsV7.makeDiv('tab');
      
      tab.append(page.attr('title') || "Page " + (p+1)).appendTo(rowDiv);

      margin -= tab.outerWidth();
      if (margin < 0) {
         rowDiv.addClass('back-row');
         rowDiv = TabsV7.makeDiv('row').insertAfter(rowDiv).append(tab);
         margin = rowDiv.outerWidth() - tab.outerWidth();  
      }
       
      tab.matchPage = page;
      tab.control = tabBlock;
      tab[0].$ = tab;
      tab.bind('click', function() {
         var $this = this.$;
            
         $this.control.selectedTab.removeClass('selected-tab').matchPage
          .removeClass('selected-page');
         $this.addClass('selected-tab').matchPage.addClass('selected-page');
         $this.control.selectedTab = $this;
      });
   });
};

$(function() {
   TabsV7.tabify($('#block1'));
});