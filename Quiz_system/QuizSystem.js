load("Quiz.js");
load("Question.js");

var QuizSystem = function() {
	var line, args, quizArr = {};

	do {
		print("QuizSystem> ");

		if ((line = readline()) && line !== "done") {
			args = line.trim().split(" ");
			if (args.length == 2) {
				if (args[0] === "new") {
					quizArr[args[1]] = new Quiz(args[1]);
					quizArr[args[1]].edit();
				}
				else if (args[0] === "edit") {
					if(quizArr[args[1]])
						quizArr[args[1]].edit();
					else
						print("Unknown quiz " + args[1]);
				}	
				else if (args[0] === "del")
					delete(quizArr[args[1]]);
				else if (args[0] === "run") {
					if(quizArr[args[1]])
						quizArr[args[1]].run();
					else
						print("Unknown quiz " + args[1]);
				}
				else
					print("Error: Command format <new/edit/del/run> <quiz name>")
			}
			else
				print("Error: Command format <action> <quiz name>");
		}
	} while (line !== "done");

};

QuizSystem();