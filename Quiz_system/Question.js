var Question = function() {
	
	this.score = 0;
	this.name = "";
	this.description = "";
}

Question.prototype.questionConfig = function() {
	print("Enter point value: ");
	this.score = parseInt(readline(), 10);
	print("Enter name: ");
	this.name = readline();
	print("Enter description: ");
	this.description = readline();
}

Question.prototype.getName = function() {
	return this.name;
};

Question.prototype.getDescription = function() {
	return this.description;
};

Question.prototype.getScore = function() {
	return this.score;
};