load("Question.js");
load("Answer.js");

var NumQuestion = function() {
	var nq = new Question();

	nq.min = 0;
	nq.max = 0;
	nq.response = 0;

	nq.config = function() {
		var line, args, badInput = true, num1, num2;

		this.questionConfig();
		do {
			print("Enter a min/max range, separated by pipe, with min <= max: ");
			if (line = readline()) {
				args = line.split('|');
				if (args.length == 2 && !isNaN(num1 = parseInt(args[0], 10)) 
				 && !isNaN(num2 = parseInt(args[1], 10))) {
					if (num1 <= num2) {
						badInput = false;
						this.min = num1;
						this.max = num2;
					}
				}
			}
		} while (badInput);
	};

	nq.prompt = function() {
		var line;

		print(this.description);
		print("Value: " + this.score);
		do {
			print("Enter numerical value: ");
			line = readline();
			this.response = parseInt(line, 10);
		} while (isNaN(this.response));
	};

	nq.getAnswer = function() {
		var ans = new Answer();

		if (this.response && this.response >= this.min && this.response <= this.max) {
			ans.comment = "Correct";
			ans.score = this.score;
		}
		else
			ans.comment = "Correct answer is between " + this.min + " and " + this.max;

		this.response = 0;

		return ans;
	};

	return nq;
}

