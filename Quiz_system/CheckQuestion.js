load("Question.js");
load("Answer.js");

var CheckQuestion = function() {
    var cq = new Question();

    cq.altArr = [];
    cq.choices = [];    //Correct choices
    cq.response = [];

    cq.config = function() {
        var line, i, badInput = true, args, temp;

        this.questionConfig();
        i = 0;
        print("Enter alternatives, one per line, ending with a blank line. ");
        while (line = readline())
            this.altArr[i++] = line;

        do {
            print("Enter zero or more choices, single-blank separated, each between 1 and " + this.altArr.length);

            if (line = readline()) {
                args = line.split(' ');
                badInput = false;

                for (var j = 0; j < args.length && !badInput; j++) {
                    if (!isNaN(temp = parseInt(args[j], 10))) {
                        if (temp < 1 || temp > this.altArr.length)
                            badInput = true;
                        else
                            this.choices[j] = temp;
                    }
                    else
                        badInput = true;
                }
            }
        } while (badInput && line);
    };

    cq.prompt = function() {
        var line, badInput;

        print(this.description);
        print("Value: " + this.score);
        for (var i = 0; i < this.altArr.length; i++)
            print((i + 1).toString() + ". " + this.altArr[i]);
        do {
            badInput = false;
            print("Enter zero or more choices, single-blank separated, each between 1 and " + this.altArr.length);
            if (line = readline()) {
                this.response = line.split(' ');
                for (var i = 0; i < this.response.length; i++)
                    if (this.response[i] < 1 || this.response[i] > this.altArr.length)
                        badInput = true;
            }
        } while(badInput);
    };

    cq.getAnswer = function() {
        var ans = new Answer();
        var missed = this.choices.length;
        var incorrect = 0;

        for (var i = 0; i < this.response.length; i++) {
            if (this.choices.indexOf(parseInt(this.response[i], 10)) !== -1)
                missed--;
            else
                incorrect++;
        }
        if (incorrect === 0 && missed === 0) {
            ans.comment = "Correct";
            ans.score = this.score;
        }
        else
            ans.comment = "You selected " + incorrect + " incorrect items and missed " + missed + " correct ones";

        this.response = [];

        return ans;
    };

    return cq;
}