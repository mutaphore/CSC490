load("Question.js");
load("Answer.js");

var MCQuestion = function() {
	var mcq = new Question();

	mcq.altArr = [];
	mcq.choice = 0;		//Correct choice
	mcq.response = 0;

	mcq.config = function() {
		var line, i;

		this.questionConfig();
		i = 0;
		do {
			print("Enter alternative " + (i + 1).toString());
			if (line = readline())
				this.altArr[i++] = line;		
		} while(line);

		do {
			print("Enter correct choice, from 1 to " + this.altArr.length);
		} while (!(line = readline()) || (parseInt(line, 10) < 1
		 || parseInt(line, 10) > this.altArr.length));

		this.choice = parseInt(line, 10);
	};

	mcq.prompt = function() {
		var line;

		print(this.description);
		print("Value: " + this.score);
        for (var i = 0; i < this.altArr.length; i++)
            print(String.fromCharCode(65 + i) + ". " + this.altArr[i]);

		do {
			print("Enter A to " + String.fromCharCode(this.altArr.length + 64) + ": ");
			line = readline();
			this.response = line.charCodeAt(0) - 64;
		} while (this.response < 1 || this.response > this.altArr.length);
	}

	mcq.getAnswer = function() {
		var ans = new Answer();

		if (this.response && this.response == this.choice) {
			ans.score = this.score;
			ans.comment = "Correct";
		}
		else
			ans.comment = "The right answer is " + String.fromCharCode(this.choice + 64);

		this.response = 0;

		return ans;
	}

	return mcq;
}
