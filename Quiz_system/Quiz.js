load("NumQuestion.js");
load("MCQuestion.js");
load("CheckQuestion.js");

var Quiz = function(quizName) {

	this.quizName = quizName;
	this.questionArr = [];
}

Quiz.prototype.edit = function() {
	var line, args, i = this.questionArr.length, ans, temp;

	do {
		print("Enter command for " + this.quizName);

		if ((line = readline()) && line !== "done") {
			args = line.trim().split(" ");
			if (args.length == 2) {
				if (args[0] === "add") {
					if (args[1] === "mc") {
						this.questionArr[i] = new MCQuestion();
						this.questionArr[i++].config();
					}
					else if (args[1] === "num") {
						this.questionArr[i] = new NumQuestion();
						this.questionArr[i++].config();
					}
					else if (args[1] === "check") {
						this.questionArr[i] = new CheckQuestion();
						this.questionArr[i++].config();	
					}
				}
				else if (args[0] === "raise") {
					if (args[1] - 1 > 0) {
						temp = this.questionArr[args[1]-1];
						this.questionArr[args[1]-1] = this.questionArr[args[1]-2];
						this.questionArr[args[1]-2] = temp;
					}
				}
				else if (args[0] === "lower") {
					if (args[1] - 1 < this.questionArr.length - 1) {
						temp = this.questionArr[args[1]-1];
						this.questionArr[args[1]-1] = this.questionArr[args[1]];
						this.questionArr[args[1]] = temp;
					}
				}
				else if (args[0] === "try") {
					if (this.questionArr[args[1]-1]) {
						this.questionArr[args[1]-1].prompt();
						ans = this.questionArr[args[1]-1].getAnswer();
						print("Answer gets score " + ans.score + " and comment " + ans.comment);
					}
				}
				else if (args[0] === "drop")
					this.questionArr.splice([args[1]-1], 1);
			}
			else if (args.length == 1 && args[0] === "show") {
				for (var j = 0; j < this.questionArr.length; j++)
					print((j + 1) + ". " + this.questionArr[j].getName());
			}
		}
	} while (line != "done");
}

Quiz.prototype.run = function() {
	var totPoints = 0;

	for (var i = 0; i < this.questionArr.length; i++) {
		this.questionArr[i].prompt();
		ans = this.questionArr[i].getAnswer();
		print("Answer gets score " + ans.score + " and comment " + ans.comment);
		totPoints += ans.score;
	}
	print("Total points: " + totPoints);
}
