var Params = function() {
   this.maxChar = null;
   this.length = 0;
   this.randRange = 0;
   this.seed = 0;

   this.read = function() {
      var line, ok = false;

      do {
         print("Enter max character, number of characters, and seed");
         line = readline().trim().split(' ');
         if (line.length != 3)
            print("Must have three entries");
         else {
            this.maxChar = line[0].toUpperCase().charAt(0);
            this.length = parseInt(line[1]);
            this.seed = parseInt(line[2]);

            if (this.maxChar < "A" || this.maxChar > "F")
               print("Max char must be between A and F");
            else if (!this.length || this.length > 10)
               print("Number of chars must be between 1 and 10");
            else if (!this.seed || this.seed < 0)
               print("Enter a nonnegative integer for seed");
            else {
               this.randRange
                = this.maxChar.charCodeAt(0) - 'A'.charCodeAt(0) + 1;
               ok = true;
            }
         }
      } while (!ok)
   }

}
