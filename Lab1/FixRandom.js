Math.seed = 0;

Math.random = function () {

   if (arguments.length > 0)
      Math.seed = arguments[0];
   else {
      Math.seed = (Math.seed * 131071 + 524287) % 8191;
      return Math.seed / 8192;
   }
};
