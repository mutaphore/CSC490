var Pattern = function(params) {

   this.letters = [];

   this.read = function() {
      var line, letter, errors;

      do {
         errors = 0;
         if (line = readline()) {
            this.letters = line.trim().toUpperCase().split(' ');
            for (var i = 0; i < this.letters.length; i++) {
               letter = this.letters[i];
               if (letter.length != 1 || letter < 'A' ||
                letter > params.maxChar) {
                  print(this.letters[i], " is not a valid guess");
                  errors++;
               }
            }
            if (this.letters.length < params.length) {
               print("Guess is too short");
               errors++;
            }
            else if (this.letters.length > params.length) {
               print("Guess is too long");
               errors++;
            }
         }
      } while (line && errors);
   }

   this.randomize = function() {
      for (var i = 0; i < params.length; i++) {
         this.letters[i] = String.fromCharCode('A'.charCodeAt(0)
          + Math.random() * params.randRange);
      }
   }

   this.toString = function() {
      return this.letters.join(" ");
   }

   this.match = function(pattern) {
      var result = {exact: 0, inexact: 0};
      var mMatches = [], gMatches = [];

      for (var i = 0; i < this.letters.length; i++)
         if (this.letters[i] === pattern.letters[i]) {
            mMatches[i] = gMatches[i] = true;
            result.exact++;
         }

      for (var i = 0; i < this.letters.length; i++)
         for (var j = 0; j < pattern.letters.length; j++)
            if (!mMatches[i] && !gMatches[j]
                && this.letters[i] === pattern.letters[j]) {
               mMatches[i] = gMatches[j] = true;
               result.inexact++;
            }

      var retObj = function(result, length) {
         this.imperfect = function() {
            return result.exact < length;
         }

         this.toString = function() {
            return result.exact.toString().concat(" exact and ",
             result.inexact.toString(), " inexact.");
         }
      }

      return new retObj(result, params.length);
   }

}
