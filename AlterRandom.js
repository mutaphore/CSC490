
Math.s = 0;
Math.random = function () {
   var r;

   if (arguments.length == 1)
      Math.s = arguments[0];
   else {
      Math.s = (Math.s * 131071 + 524287) % 8191;
      r = Math.s / 8192;
   }
   return r;
};

(function() {
   var rnd;

   for (var i = 0; i < 2; i++) {
      Math.random(1);  // Set seed to 1
      do {
         print(rnd = Math.random());
      } while (rnd < .90);
   }
})();
