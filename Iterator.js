load("LinkedList4.js");

LinkedList.prototype.iterator = function() {
   var current = this.head;

   return function(val) {
      var rtn = current && current.data;

      current = current && current.next;
      return rtn;
   }
};

(function() {
   var val, list = new LinkedList();

   [42, 14, 100].forEach(function(val) {list.add(val);});
   for (var itr = list.iterator(); val = itr();)
      print(val);
})();
