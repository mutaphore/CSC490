$.extend({makeExpander: function(root) {
  var children = root.children(), i1, i2, title, exp, item;

  if (children.length > 0) {
    root.addClass("root");

    children.each(function(p) {
      i1 = $('<img class="icon" src="up.png">').bind('click', function() {
        (item = $(this).parent().parent()).insertBefore(item.prev()); });

      i2 = $('<img class="icon" src="down.png">').bind('click', function() {
        (item = $(this).parent().parent()).insertAfter(item.next()); });

      title = $('<div class="title-green"></div>').attr('title', 
       $(this).attr('title')).text($(this).attr('title')).bind('click', 
        function() { $(this).parent().next().toggle(); 
          $(this).toggleClass("title-red"); });

      exp = $('<div class="exp"></div>').append(i1).append(i2).append(title);

      $(this).css("margin-left", "18px")
       .wrap('<div class="item-block"></div>').before(exp);
    });
  }
}})