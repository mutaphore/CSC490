var readRecords = function() {
   var ln, nm, records = [];

   var i = 0;
   while (ln = readline()) {
      ln = ln.split("|");
      nm = ln[1].split(",");
      records[i] =
       {id: ln[0], name: {first: nm[1].trim(), last: nm[0].trim()},
        classes: ln[2].split(",")};
      if (nm[2])
         records[i].name.middle = nm[2].trim();
      i++;
   }

   return records;
};

var printRecords = function(records) {
   var rec;

   for (var i = 0; i < records.length; i++) {
      rec = records[i];
      print(rec.id + " Name: " + rec.name.first + " " + (rec.name.middle ? rec.name.middle + " " : "")
       + rec.name.last + ":");
      for (var j = 0; j < rec.classes.length; j++)
         print(rec.classes[j].trim());
      print();
   }
};

printRecords(readRecords());
