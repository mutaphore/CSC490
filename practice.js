makeDeque = function() {
   var deque = {};
   Array.apply(deque);
   deque.pushTail = Array.prototype.push;
   deque.pushHead = Array.prototype.unshift;
   deque.popTail = Array.prototype.pop;
   deque.popHead = Array.prototype.shift;
   deque.isEmpty = function() {
      return this.length === 0;
   }
   return deque;
};

(function() {
   var dq = makeDeque();

   dq.pushTail(4);
   dq.pushHead(3);
   dq.pushHead(2);
   dq.pushHead("one");
   dq.pushTail("five");
   print("length " + dq.length + "last item: " + dq.popTail());
   while (!dq.isEmpty())
      print(dq.popHead());
})();
